#include "alta/view/ProjectDialog.cpp"

#include "alta/view/ProjectWindow.cpp"

#include "alta/view/Project.cpp"

#include "alta/view/Application.cpp"

#include "alta/view/View.cpp"

#include "alta/view/DeveloperSettings.cpp"

#include "alta/view/ReloadableQuickView.cpp"

#include "alta/main.cpp"
