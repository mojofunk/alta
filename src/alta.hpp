#ifndef ALTA_ALTA_H
#define ALTA_ALTA_H

#include "alta/view/header_includes.hpp"

#include "alta/view/ReloadableQuickView.hpp"

#include "alta/view/ProjectDialog.hpp"

#include "alta/view/ProjectWindow.hpp"

#include "alta/view/Project.hpp"

#include "alta/view/Application.hpp"

#include "alta/view/DeveloperSettings.hpp"

#include "alta/view/View.hpp"

#endif // ALTA_ALTA_H
