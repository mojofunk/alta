#include "alta.hpp"

int
main( int argc, char** argv )
{
	adt::Log::set_all_categories_enabled( true );
	adt::Log::set_all_types_enabled( true );

	qadt::Application qadt_app( &argc, &argv );

	A_REGISTER_THREAD( "Main Thread", adt::ThreadPriority::NORMAL );

	// todo alta::model::Application model_application;

	alta::view::Application app( argc, argv );

	qadt::Application::developerWindow()->show();

	alta::view::View view;

	int result = view.run();

	return result;
}
