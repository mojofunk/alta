#pragma once

#include "header_includes.hpp"

namespace alta::view
{

class DeveloperSettings : public QObject
{
public:
	Q_OBJECT

public:
	DeveloperSettings();

	QString GetQmlRootPath();

private:
	QString qmlRootPath;

private:
	A_DECLARE_CLASS_MEMBERS( DeveloperSettings )
};

} // namespace alta::view
