#pragma once

#include "header_includes.hpp"

#include "ReloadableQuickView.hpp"

namespace alta::view
{

class ProjectDialog : public ReloadableQuickView
{
	Q_OBJECT;

public:
	ProjectDialog();
	~ProjectDialog();

private:
	A_DECLARE_CLASS_MEMBERS( ProjectDialog )
};

} // namespace alta::view
