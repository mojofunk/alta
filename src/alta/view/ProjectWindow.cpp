#include "alta.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::ProjectWindow )

ProjectWindow::ProjectWindow()
    : ReloadableQuickView( "ProjectWindow.qml" )
{
	setResizeMode( QQuickView::SizeRootObjectToView );
	QString resource_path = QStringLiteral( "qrc:/" ) + getQmlFileName();
	setSource( QUrl( resource_path ) );
}

bool
ProjectWindow::event( QEvent* event )
{
	A_CLASS_CALL();

	if ( event->type() == QEvent::Close ) {
#if 0
		// abstract this to return mProject.Close(); ?
		if( mProject.UnsavedChanges() )
		{
			ConfirmSaveChangesDialog dialog;
			const auto result = dialog.run();
			if (result == cancel)
				{
				return;
				}
			else if (result == save)
				{
				mProject.Save();
				}
			mProject.Close();
		}
#endif
	}
	return QQuickView::event( event );
}

} // namespace alta::view