#pragma once

#include "header_includes.hpp"

namespace alta::view
{

class DeveloperSettings;
class ProjectDialog;
class Project;

// todo Move all this to view::Application?
class View : public QObject
{
	Q_OBJECT;

public:

	// The View should really take a reference to the Model connect
	// listeners/callbacks to it
	View();
	~View();

public:

	int run();

public:
	void on_action_new_project();
	void on_action_open_project();

	//	void on_action_close_project();

	void on_action_quit();

	void on_action_preferences();

public:

	Q_INVOKABLE void new_project();

	void open_project( const mojo::fs::path& path );

	//	void close_project(Project);

	Q_INVOKABLE void show_project_dialog();

private: // Data
	std::unique_ptr<DeveloperSettings> m_developer_settings;

	std::unique_ptr<ProjectDialog> m_project_dialog;

	std::vector<std::unique_ptr<Project>> m_projects;

private:
	A_DECLARE_CLASS_MEMBERS( alta::view::View )
};

} // namespace alta::view