#include "source_includes.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::Application )

Application* Application::m_instance = nullptr;

Application::Application( int& argc, char** argv )
    : QApplication::QApplication( argc, argv )
{
	assert( !m_instance );
	m_instance = this;
}

Application::~Application()
{
	m_instance = nullptr;
}

} // namespace alta::view