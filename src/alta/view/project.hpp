#pragma once

#include "header_includes.hpp"

namespace alta::view
{

class View;
class ProjectWindow;

/**
 * Project holds the strong reference to the Project and all of the
 * Widgets and Windows associated with a Project.
 *
 * Child Widgets that need to access other widgets associated with the
 * Project can do so via a reference to Project if necessary.
 */
class Project : public QObject
{
	Q_OBJECT;

public:

	Project( View& view );

	~Project();

public: // actions
	void on_action_add_audio_track();

	void on_action_add_instrument_track();

	void on_action_add_midi_track();

	void on_action_import_audio_files();

	void on_action_import_midi_files();

	// Transport Actions

	void on_action_play();

	void on_action_stop();

	void on_action_toggle_play();

	void on_action_toggle_record();

	void on_action_toggle_loop();

public:
	//	mojo::Project& project() { return *m_project.get(); }

public:
	ProjectWindow& project_window() { return *m_project_window.get(); }

private:
	//	std::unique_ptr<mojo::Project> m_project;

private:

	View& m_view;

	std::unique_ptr<ProjectWindow> m_project_window;

	//	std::unique_ptr<ImportFileDialog> m_import_file_dialog;

private:
	A_DECLARE_CLASS_MEMBERS( alta::view::Project )
};

} // namespace alta::view
