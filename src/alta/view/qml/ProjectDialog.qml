﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: root
    width: 400 // alta.ProjectDialog.defaultWidth
    height: 480 // alta.ProjectDialog.defaultHeight
    visible: true

    ColumnLayout {
        anchors.centerIn: parent
        TextField {
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Project Name")
            placeholderText: qsTr("New Project Name")
        }
        Button {
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Create Project")

            onClicked: {
                view.new_project();
            }
        }
    }
}