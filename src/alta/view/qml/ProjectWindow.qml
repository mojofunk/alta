import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: root
    width: 640
    height: 480
    visible: true

    anchors.fill: parent

ColumnLayout {
    // anchors.centerIn: parent
    MenuBar {
        Layout.fillWidth: true
        Menu {
            title: qsTr("&File")
            Action { text: qsTr("&New Project...") }
            Action { text: qsTr("&Open Project...") }
            Action { text: qsTr("&Save Project") }
            Action { text: qsTr("Save &As...") }
            MenuSeparator { }
            Action { text: qsTr("&Close") }
            Action { text: qsTr("&Quit") }
        }
        Menu {
            title: qsTr("&Edit")
            Action { text: qsTr("Cu&t") }
            Action { text: qsTr("&Copy") }
            Action { text: qsTr("&Paste") }
        }
        Menu {
            title: qsTr("&Help")
            Action { text: qsTr("&About") }
        }
    }

    ToolBar {
        Layout.fillWidth: true
        RowLayout {
            anchors.fill: parent
            ToolButton {
                text: qsTr("‹")
                onClicked: stack.pop()
            }
            Label {
                text: "ToolBar"
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            ToolButton {
                text: qsTr("⋮")
                //onClicked: menu.open()
            }
        }
    }

    TextField {
        text: qsTr("Placeholder")
        placeholderText: qsTr("User name")
        anchors.centerIn: parent
    }
}
}