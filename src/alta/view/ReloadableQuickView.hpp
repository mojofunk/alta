#pragma once

#include "header_includes.hpp"

namespace alta::view
{

class ReloadableQuickView : public QQuickView
{
	Q_OBJECT

public:
	ReloadableQuickView( QString file_name );
	~ReloadableQuickView();

	void keyPressEvent( QKeyEvent* e );

	static void setRootSourcePath( QString path );

	QString getQmlFileName() const;

public slots:
	void reloadQmlFromDisk();

signals:
	void qmlReloaded();

protected:
	static QString s_RootSourcePath;
	QString mQmlFileName;

private:
	A_DECLARE_CLASS_MEMBERS( ReloadableQuickView )
};

} // namespace alta::view
