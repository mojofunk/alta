#include "source_includes.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::ReloadableQuickView )

QString ReloadableQuickView::s_RootSourcePath = QString();

ReloadableQuickView::ReloadableQuickView( QString file_name )
    : mQmlFileName( file_name )
{
}

ReloadableQuickView::~ReloadableQuickView() {}

void
ReloadableQuickView::keyPressEvent( QKeyEvent* e )
{
	A_CLASS_CALL();

	if ( e->key() == Qt::Key_F5 ) {
// #ifdef QT_DEBUG // only invoke the reload in debug mode...
		QMetaObject::invokeMethod( this, "reloadQmlFromDisk", Qt::QueuedConnection );
// #endif
	}

	QQuickView::keyPressEvent( e );
}

void
ReloadableQuickView::setRootSourcePath( QString path )
{
	A_CLASS_STATIC_CALL();

	s_RootSourcePath = path;
}

QString
ReloadableQuickView::getQmlFileName() const
{
	A_CLASS_CALL();

	return mQmlFileName;
}

void
ReloadableQuickView::reloadQmlFromDisk()
{
	A_CLASS_CALL();

	// Initially, the application loads the QML files from the embedded resources.
	// On F5 reload, it switches to loading directly from the QML source files on
	// disk. This allows a refresh without restarting the application.
	engine()->clearComponentCache();

	QString qmlFilePath = s_RootSourcePath + "/" + mQmlFileName;

	A_CLASS_DATA1( qmlFilePath.toStdString() );

	setSource( QUrl::fromLocalFile( qmlFilePath ) );

	emit qmlReloaded();
}

} // namespace alta::view