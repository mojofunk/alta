#include "source_includes.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::ProjectDialog )

ProjectDialog::ProjectDialog()
    : ReloadableQuickView( "ProjectDialog.qml" )
{
	setResizeMode( QQuickView::SizeRootObjectToView );
	QString resource_path = QStringLiteral( "qrc:/" ) + getQmlFileName();
	setSource( QUrl( resource_path ) );
}

ProjectDialog::~ProjectDialog() {}

} // namespace alta::view