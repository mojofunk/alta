#include "source_includes.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::DeveloperSettings )

DeveloperSettings::DeveloperSettings() {
	const auto app_data_path =
	    mojo::get_special_path( mojo::SpecialPathType::UserDataDirectory );

	const auto dev_settings_file_path =
	    app_data_path / "Alta" / "DeveloperSettings.json";

	const auto utf8_path = dev_settings_file_path.u8string();

	QFile file( QString::fromStdString( utf8_path ) );

	if ( !file.open( QFile::ReadOnly ) ) {
		A_CLASS_MSG(
		    A_FMT( "Unable to open developer settings file: {}", utf8_path ) )
		return;
	}

	QJsonParseError error;
	QJsonDocument doc = QJsonDocument::fromJson( file.readAll(), &error );
	file.close();
	if ( error.error != QJsonParseError::NoError ) {
		A_CLASS_MSG( A_FMT( "Error reading developer settings file : {}",
		                    error.errorString().toUtf8().constData() ) );
		return;
	}

	QJsonObject json = doc.object();

	qmlRootPath = json.value( "qml_root_path" ).toString();

	const std::string qml_root_path = qmlRootPath.toUtf8().constData();

	A_CLASS_MSG( A_FMT( "Using qmlRootPath :  {}", qml_root_path ) );
}

QString
DeveloperSettings::GetQmlRootPath()
{
	return qmlRootPath;
}

} // namespace alta::view