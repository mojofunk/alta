#pragma once

#include "header_includes.hpp"

#include "ReloadableQuickView.hpp"

namespace alta::view
{

class ProjectWindow : public ReloadableQuickView
{
	Q_OBJECT;

public:
	ProjectWindow();

	bool event( QEvent* event ) override;

private:
	A_DECLARE_CLASS_MEMBERS( ProjectWindow )
};

} // namespace alta::view