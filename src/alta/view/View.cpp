#include "source_includes.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::View )

View::View()
    : m_developer_settings( new DeveloperSettings() )
    , m_project_dialog( new ProjectDialog() )
{
	ReloadableQuickView::setRootSourcePath(
	    m_developer_settings->GetQmlRootPath() );


	m_project_dialog->engine()->rootContext()->setContextProperty( "view", this );
	// assert( !m_instance );
	// m_instance = this;
}

View::~View()
{
	// m_instance = nullptr;
}

int
View::run()
{
	A_CLASS_CALL();

	m_project_dialog->show();

	return QApplication::exec();
}

void
View::new_project()
{
	A_CLASS_CALL();

	// todo call into model and do this in a callback
	m_projects.emplace_back( std::make_unique<Project>( *this ) );
}

void
View::show_project_dialog()
{
	A_CLASS_CALL();

	m_project_dialog->show();
}

} // namespace alta::view