#include "alta.hpp"

namespace alta::view
{

A_DEFINE_CLASS_MEMBERS( alta::view::Project )

// Take ownership of the session
Project::Project( View& view )
    : m_view( view )
    , m_project_window( new ProjectWindow() )
    //, m_import_file_dialog(new ImportFileDialog(*this))
{
	m_project_window->engine()->rootContext()->setContextProperty( "view", &view );
	m_project_window->show();

	// todo close project when project window closed so that Project
	// is removed from Application/View

	//Application::get_instance().add_window(*m_project_window);
}

Project::~Project()
{
}

void
Project::on_action_add_audio_track()
{
	A_CLASS_CALL();

	/**
	AddAudioTrackDialog dialog(project_window);
	dialog->show();

	if (dialog.exec() != QDialog::Accepted) {
	  return;
	}

	auto tracks = m_project->new_audio_track(2, 2, nullptr, 1, "Audio", 0);

	Or let Application connectd to use a mojo::TaskManager?

	auto task = m_project->new_audio_tracks(new_track_options);

	TaskDialog task_dialog(task);
	task_dialog.exec();
	*/
}

void
Project::on_action_add_instrument_track()
{
	A_CLASS_CALL();
}

void
Project::on_action_add_midi_track()
{
	A_CLASS_CALL();
}

void
Project::on_action_import_audio_files()
{
	A_CLASS_CALL();

	/**
	ImportAudioDialog dialog(project_window);

	dialog.show();

	if (dialog.exec() != QDialog::Accepted) {
	 return;
	}

	auto import_options = dialog.get_import_options();

	auto task = m_project->import_audio_files(import_options);

	AudioImportTaskDialog task_dialog(task);
	task_dialog.exec();
	 */
}

void
Project::on_action_import_midi_files()
{
	A_CLASS_CALL();

	//	m_import_file_dialog->run();
}

void
Project::on_action_play()
{
	A_CLASS_CALL();
}

void
Project::on_action_stop()
{
	A_CLASS_CALL();
}

void
Project::on_action_toggle_play()
{
	A_CLASS_CALL();
}

void
Project::on_action_toggle_record()
{
	A_CLASS_CALL();
}

void
Project::on_action_toggle_loop()
{
	A_CLASS_CALL();
}

} // namespace alta::view