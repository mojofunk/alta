#pragma once

#include "header_includes.hpp"

namespace alta::view
{

class ProjectDialog;
class Project;

class Application : public QApplication
{
public:
	Application( int& argc, char** argv );
	~Application();

private:
	static Application* m_instance;

public:
	static Application* instance() { return m_instance; }

private:
	A_DECLARE_CLASS_MEMBERS( alta::view::Application )
};

} // namespace alta::view