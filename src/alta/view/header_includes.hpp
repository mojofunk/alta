#pragma once

#include <mojo-core.hpp>

#include <QtWidgets>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQuickStyle>

// TODO ifdef DEV_TOOLS_ENABLED
#include <qadt.h>
