if(NOT TARGET qadt-0::qadt-0)
    find_package(qadt-0 REQUIRED)
endif()

if(NOT TARGET mojo-0::mojo-core-0)
    find_package(mojo-0 REQUIRED)
endif()

find_package(Qt5 REQUIRED COMPONENTS Core Qml Quick QuickControls2 Widgets)

if(ALTA_USE_QTQUICK_COMPILER)
    find_package(Qt5QuickCompiler)
endif()
