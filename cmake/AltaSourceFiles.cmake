file(GLOB ALTA_VIEW_HEADERS "src/alta/view/*.hpp")
file(GLOB ALTA_VIEW_SOURCES "src/alta/view/*.cpp")

set(ALTA_UNITY_SOURCE "src/alta.cpp")

set(ALTA_QML_QRC alta.qrc)

set(ALTA_HEADERS
	"${ALTA_VIEW_HEADERS}"
	src/alta.hpp
)

set(ALTA_SOURCES
    ${ALTA_HEADERS}
	${ALTA_UNITY_SOURCE}
)
