set(ALTA_TARGET alta-${ALTA_VERSION_MAJOR})

if(ALTA_USE_QTQUICK_COMPILER)
    qtquick_compiler_add_resources(ALTA_QML_RESOURCES_CPP ${ALTA_QML_QRC})

    list(APPEND ALTA_SOURCES
        ${ALTA_QML_RESOURCES_CPP}
    )
else()
    list(APPEND ALTA_SOURCES
        ${ALTA_QML_QRC}
    )
endif()

add_executable(${ALTA_TARGET}
    ${ALTA_SOURCES}
)

set_property(TARGET ${ALTA_TARGET} PROPERTY AUTOMOC ON)
set_property(TARGET ${ALTA_TARGET} PROPERTY AUTORCC ON)

set_target_properties(${ALTA_TARGET} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    VERSION ${ALTA_VERSION}
    SOVERSION ${ALTA_VERSION_MAJOR}
)

target_include_directories(${ALTA_TARGET} PUBLIC src)

target_link_libraries(${ALTA_TARGET}
    PUBLIC
    mojo-0::mojo-core-0
    qadt-0::qadt-0
    Qt5::Core
    Qt5::Widgets
    Qt5::Qml
    Qt5::Quick
    Qt5::Network
    Qt5::QuickControls2
    Qt5::Gui
    Qt5::WinMain
)
